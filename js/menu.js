$(function(){
	$('#menu ul li ul').hide();

	$('#menu ul li').hover(
		function(){
			$('#menu ul li').not($('ul', this)).stop();

			$('ul li:first-child', this).before(
				'<li class="arrow">arrow</li>'
			);

			$('ul li.arrow', this).css('border-bottom', '0');

			$('ul', this).show();
		},

		function(){
			$('ul', this).hide();

			$('ul li.arrow', this).remove();
		}
	);

});